from flask import render_template, request, send_from_directory
import json
from types import SimpleNamespace as Namespace
import os

from app import app

# Application page
@app.route('/', methods=["GET"])
def index():
    with open(os.path.join(app.static_folder,'en.json')) as f:
        data = json.loads(f.read(), object_hook=lambda d: Namespace(**d))
    return render_template("index.html", data=data)


# Application page in Spanish, client decided to wait on this functionality
@app.route('/es', methods=["GET"])
def es():
    with open(os.path.join(app.static_folder,'en.json')) as f:
        data = json.loads(f.read(), object_hook=lambda d: Namespace(**d))
    return render_template("index.html", data=data)


# Set routes for crawlers to discover SEO files
@app.route('/robots.txt')
@app.route('/sitemap.xml')
def static_from_root():
    return send_from_directory(app.static_folder, request.path[1:])

# Handle 404 error
@app.errorhandler(404)
def error404(e):
    return render_template("404.html", e=e), 404
