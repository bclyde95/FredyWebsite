const accordionTitle = document.getElementsByClassName('uk-accordion-title');

if (accordionTitle.ariaExpanded == 'true') {
  accordionTitle.classList.add('uk-accordion-title-expanded');
}

const mapsSelector = () => {
  if /* if iOS, open in Apple Maps */
    ((navigator.platform.indexOf("iPhone") != -1) ||
    (navigator.platform.indexOf("iPad") != -1) ||
    (navigator.platform.indexOf("iPod") != -1))
    window.open("maps://goo.gl/maps/1yMqi7k2C2ACWQiJ7");

  else /* else use Google */
    window.open("https://goo.gl/maps/1yMqi7k2C2ACWQiJ7");
}


// Preload images
let images = [];
((...args) => {
  for (let i = 0; i < args.length; i++) {
    images[i] = new Image();
    images[i].src = args[i];
  }
})(
  "/static/images/landing.webp",
  "/static/images/trimming.webp",
  "/static/images/cleaning.webp",
  "/static/images/mowing.webp",
  "/static/images/weedRemoval.webp"
);
