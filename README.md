# FredysWebsite
#### Rosales landscaping and maintenance @ klamathlandscaping.com

## Steps to clone
1. run `git clone https://github.com/bclyde95/FredyWebsite`
2. Change directory to FredyWebsite `cd FredyWebsite`
3. Create a python virtual environment `python -m venv venv`
4. Activate environment `source venv/bin/activate`
5. Install dependencies `pip install -r requirements.txt`
6. Set up is complete
